﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIEventScript : MonoBehaviour
{
    public GameObject[] panels;
    int page = 0;
    public void ChangePage(int change)
    {
        panels[page].SetActive(false);

        page += change;

        if (page < 0)
        {
            page = panels.Length + page;
        }

        page = page % panels.Length;

        panels[page].SetActive(true);
    }

    // Update is called once per frame

}
